---
title: "😈 I know you're using Windows and Chrome 😈"
collection: computing
permalink: /computing/i-know-youre-using-windows-and-chrome
author: Mr T
date: 2022-05-28
tags: [Computing]
img: windowschrome.png

---

Stop it. Use BSD or linux. Because I can see what you are doing on your computer. 

No, I'm just kidding. But, we could use some more operating system diversity. It never hurts using a different operating system because rather than all people being affected using the same OS, only some people do. 

I know Linux is just a kernel. 

By the way, Windows is very odd in the way microsoft has handled it now. I honestly don't like the way they handled privacy with Windows. I know that people think privacy is a joke with computing. Why does privacy even matter? I just feel that privacy is possible without relying on monetary value for providing something like an OS. An OS is only, and is not a service. It is literally stored locally on your computer. At least I guess microsoft has the gaming front.  
