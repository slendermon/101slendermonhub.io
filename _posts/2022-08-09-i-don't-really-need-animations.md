---
title: "i don't really need animations"
date: 2022-08-09
status: publish
permalink: /i-don't-really-need-animations
author: 'Mr T'
excerpt: ''
type: posts
tags: [rants]

---

  in terms of security, the less code the better. so, if i remove the code for animations, 
  it could have less of an attack vector? and it might run even faster, unless it is absolutely needed for animations 
  for user interactions. animations are only to make the user interface more friendly, and responsive 
  if you have a touch based gesture device, sometimes. but, animations aren't always needed, for
  example, when you click an icon on your home screen on your phone, it will load faster if there wasn't even 
  an animation just to launch that app.
  
  animations aren't always necessary, so why do i need something fancy like animations? in some cases, 
  like dragging and dropping, if that even counts as an animations, is a good use case for an animation. because it 
  shows exactly where my mouse cursor is and what it is doing. 
  
