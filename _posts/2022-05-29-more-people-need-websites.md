---
title: "🧾 More people need websites. 🧾"
collection: uncategorized
type: "uncategorized"
permalink: /uncategorized/more-people-need-websites
author: Mr T
date: 2022-05-29
img: websiteoneclean.png
tags: [Web Hosting]

---

If you're interested in computers, like me, I think it is actually really fun setting up your own website. 
