---
title: '😃 How I Made This Website 😃'
date: '2022-05-04T21:43:17+00:00'
status: publish
permalink: /how-i-made-this-website
author: 'Mr T'
excerpt: ''
type: posts
id: 1
img: webhostoneclean.png
tags: [Web Hosting] 

---

How did I make this website? I use Monero signing up for a domain service and a
web hosting &amp; VPS service. Monero is a sort of currency I recommend for privacy 
and security, because it is made to emulate cash but digitally. I use Njalla, and 1984, 
to create this website.

For Njalla, I used it for domain registration:

I made one ‘a’ DNS and connected it via www (just www).

Then, I make one ‘a’ DNS again and just use @ sign (for root).

For 1984, I used it for VPS hosting and web hosting:

Just sign up via web hosting (if ‘private’, you need monero, otherwise it won’t let you) 
and setup a CMS. A CMS is a content management system. 1984 comes with one preinstalled 
for web hosts.

<html>
  
  <audio href="/_posts" controls autostart="true" class=center> <style>.center{display:block;margin-left:auto;margin-right:auto}</style>
    <source src="/assets/audio/LOL.mp4" type="audio/mp4">
  Your browser does not support the audio element.
  </audio> <!-- audio -->

<span style=text-align:center;>i sell botatos</span>
  
</html>
