---
title: '🖥 Advice for myself on Web hosting 🖥'
date: '2022-05-12T20:58:59+00:00'
status: publish
permalink: /advice-for-myself-on-web-hosting
author: 'Mr T'
excerpt: ''
type: posts
id: 23
img: webhosttwo.png
tags: [Web Hosting] 

---

I’m thinking one day I might make ‘servers’ 
locally rather than using a VPS or web hosting. But the reason why I haven’t done that, 
is because I don’t have an ISP that allows me to host my own email/website.

Sorry this was a very short blog post. Maybe I should combine this with my 
“how to make a website” post.
